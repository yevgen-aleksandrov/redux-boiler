#  redux boiler

# Get Started
1. `npm install`
2. Launch environment:
*  **Production**: `npm start`
*  **Development**: `npm run dev`
3. Build for production:
* `npm run build:prod`
4. Test:
* `npm test`
