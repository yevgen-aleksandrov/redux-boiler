import React from 'react';
import { Route, IndexRoute } from 'react-router';
// components
import App from './components/App';
import NotFound from './components/NotFound';
import List from './components/List';
import SomePage from './components/SomePage';
import Details from './components/Details';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={List} />

    {/* settings */}
    <Route path="some-page" component={SomePage} onEnter={() => console.log('entered')}/>

    {/* Details */}
    <Route path="/details/:id" component={Details}/>

    {/* 404 */}
    <Route path="*" component={NotFound} />
  </Route>
);
