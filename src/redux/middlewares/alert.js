import { SITE_FETCH_DETAILS_SUCCESS } from '../actions/types.js';

const alert = ({dispatch, getState}) => next => {
  return action => {
    if (action.type === SITE_FETCH_DETAILS_SUCCESS) {
      if (action.data && action.data.id === 5) {
        console.warn('Warning! Entity with id 5 loaded!');
      }
    }
    return next(action);
  };
};

export default alert;
