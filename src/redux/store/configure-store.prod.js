import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/root-reducer';
import RavenMiddleware from 'redux-raven-middleware';
import api from '../middlewares/api';
import alert from '../middlewares/alert';

/**
 * Production Redux store
 * @param  {object} initialState    Initial state of the Redux store
 * @return {object}                 Redux store
 */
export default function configureStore (initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      thunk,
      RavenMiddleware('sentryUrl'),
      api,
	alert
    )
  );
}
