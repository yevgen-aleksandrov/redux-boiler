import {
  SITE_FETCH_USERS_SUCCESS,
  SITE_FETCH_DETAILS_SUCCESS,
  SITE_HAS_ERROR,
  SITE_SET_LOADER_STATUS,
  API
} from './types';

/**
 * cation to fetch the site data
 * @returns {Function} thunk
 */
export const siteFetchUsers = () => ({
  type   : API,
  payload: {
    url: {
      endpoint: 'https://jsonplaceholder.typicode.com/users'
    },
    success: (data) => siteFetchUsersSuccess(data),
    failure: (boolean) => siteHasError(boolean),
    loader : (boolean) => siteIsLoading(boolean)
  }
});

export const siteFetchUserDetails = (id) => ({
  type   : API,
  payload: {
    url: {
      endpoint: `https://jsonplaceholder.typicode.com/users/${id}`
    },
    success: (data) => siteFetchDetailsSuccess(data),
    failure: (boolean) => siteHasError(boolean),
    loader : (boolean) => siteIsLoading(boolean)
  }
});

/**
 * action if there is an error
 * @param bool
 * @returns {{type, hasError: *}}
 */
export const siteHasError = (bool) => ({
  type    : SITE_HAS_ERROR,
  hasError: bool
});
/**
 * action while loading
 * @param bool
 * @returns {{type, isLoading: *}}
 */
export const siteIsLoading = (bool) => ({
  type     : SITE_SET_LOADER_STATUS,
  isLoading: bool
});
/**
 * action when fetch is success
 * @param data
 * @returns {{type, data: *}}
 */
export const siteFetchUsersSuccess = (data) => ({
  type: SITE_FETCH_USERS_SUCCESS,
  data: data
});

export const siteFetchDetailsSuccess = (data) => ({
  type: SITE_FETCH_DETAILS_SUCCESS,
  data: data
});


