import { INITIAL_STATE } from '../../common/app-const';
import {
  SITE_FETCH_USERS_SUCCESS,
  SITE_FETCH_DETAILS_SUCCESS,
  SITE_HAS_ERROR,
  SITE_SET_LOADER_STATUS
} from '../actions/types';

const site = (state = INITIAL_STATE.site, action) => {
  switch (action.type) {
    case SITE_FETCH_USERS_SUCCESS: {
      if (typeof (action.data) === 'object') {
        action.data = Object.keys(action.data).map(key => action.data[key]);
      }
      return { ...state, users: action.data, details: undefined };
    }
    case SITE_FETCH_DETAILS_SUCCESS: {
      return { ...state, details: action.data };
    }
    case SITE_HAS_ERROR: {
      return { ...state, hasError: action.hasError };
    }
    case SITE_SET_LOADER_STATUS: {
      return { ...state, isLoading: action.isLoading };
    }
    default: {
      return state;
    }
  }
};

export default site;
