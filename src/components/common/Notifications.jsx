/* eslint-disable react/prop-types */
import React from 'react';
// wix-style-react
import Loader from './Loader';
// styles
let style = {
  display       : 'flex',
  justifyContent: 'center',
  alignItems    : 'center',
  position      : 'absolute',
  left          : '0',
  right         : '0',
  top           : '0',
  bottom        : '0'
};

export const Empty = () => {
  let emptyStyle = {...style,
    left  : '30%',
    right : '30%',
    top   : '30%',
    bottom: '30%'
  };
  return (
    <div style={emptyStyle}>
      {Language.get('common.notifications.empty')}
    </div>
  );
};

export const Error = (props) => (
  <div style={style}>
    {props.title || Language.get('common.notifications.error')}
  </div>
);

export const Loading = (props) => {
  style.backgroundColor = props.background ? 'rgba(255, 255, 255, 0.8)' : 'transparent';
  return (
    <div style={style}>
      <Loader size="small"/>
    </div>
  );
};

export const Notice = (props) => (
  <svg width={props.size || '15px'} height={props.size || '15px'} viewBox="0 0 15 15">
    <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="!" stroke="#f05f55">
        <g>
          <path d="M7.5,6.5 L7.5,10.5" id="Line" strokeLinecap="square"/>
          <path d="M7.5,4.5 L7.5,4.5" id="Line-Copy" strokeLinecap="square"/>
          <circle id="Oval-2" cx="7.5" cy="7.5" r="7"/>
        </g>
      </g>
    </g>
  </svg>
);
