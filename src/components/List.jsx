import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import {siteFetchUsers} from '../redux/actions/site';
import { Loading } from './common/Notifications';

class List extends React.Component {
  componentDidMount () {
    this.props.siteFetchUsers();
  }
  render () {
    let p = this.props;
    if (p.isLoading || !p.users || !p.users.length) return <Loading />
    return (
      <div>
        <div>Users list</div>
        {p.users.map(user => {
          return <div key={user.id}>
            <a href={`/details/${user.id}`}>{user.name} ({user.email})</a>
          </div>
        })}
      </div>
    );
  }
}

List.propTypes = {
  users: PropTypes.array,
  siteFetchUsers: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    users: state.site.users
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    siteFetchUsers: () => dispatch(siteFetchUsers())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
