import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { siteFetchUserDetails } from '../redux/actions/site';
import { Loading } from './common/Notifications';

class Details extends React.Component {
  componentDidMount () {
    this.props.siteFetchUserDetails();
  }

  render () {
    let p = this.props;
    if (p.isLoading || !p.details) return <Loading />
    return (
      <div>
        <div><a href="/">Back to List</a></div>
        <br/>
        <div>User Details</div>
        <br/>
         <div>
            {JSON.stringify(p.details)}
         </div>
      </div>
    );
  }
}

Details.propTypes = {
  details: PropTypes.object,
  siteFetchUserDetails: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    details: state.site.details
  };
};

const mapDispatchToProps = (dispatch, p) => {
  return {
    siteFetchUserDetails: () => dispatch(siteFetchUserDetails(p.routeParams.id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Details);
