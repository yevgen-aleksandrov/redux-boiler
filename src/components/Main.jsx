import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';


class Main extends React.Component {
  render () {
    return (
      <div>
      </div>
    );
  }
}

Main.propTypes = {
  site: PropTypes.object.isRequired
};

let mapStateToProps = (state) => {

  console.log('Main mapStateToProps', state);

  return {
    site: state.site
  };
};

export default connect(mapStateToProps)(Main);
